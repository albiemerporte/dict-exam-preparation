#include <iostream>
#include <string>
#include <cstdlib>
#include <fstream>

using namespace std;


struct NameData{
	string firstname, lastname;
};

NameData inputname(){

	NameData data;

	cout<<"Enter Sales Agent First Name: "<<endl;
	cin>>data.firstname;
	
	cout<<"Enter Sales Agent Last Name: "<<endl;
	cin>>data.lastname;
	
	return data;	
}

int main(){
	
	int opt;
	
	cout<<"Enter option: "<<endl;
	cin>>opt;
	
		if(opt == 1) {
	
				NameData nameData = inputname();
	
				cout<<"FirstName: "<<nameData.firstname<<" "<< nameData.lastname<<endl;

	
		}else if(opt == 2){
			
				ifstream file("mytext.txt");
		
				if(!file.is_open()){
					cerr<<"Error Opening File"<<endl;
					return 1;
				}
				
				string line, tosearch;
				bool found = false;
				
				cout<<"Enter Name To Search: "<<endl;
				cin>>tosearch;
				
				while(getline(file, line)){
					if(line.find("Name: " + tosearch) != string::npos){
						found = true;
						system("cls");
						cout<<line.substr(6)<<endl;
					}else if (found && line.find("Age:") != string::npos){
						cout<<line.substr(5)<<endl;
						found = false;
					}
				} // while end
				
				file.close();
				
				
		}else{
			cout<<"Invalid Choice"<<endl;
		}   // switch end
	
	return 0;
	
}
